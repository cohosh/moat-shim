package main

import (
	"crypto/rand"
	"crypto/subtle"
	"fmt"
	"io"
	"net"
	"os"
)

type ExtOrListener struct {
	net.Listener
	authCookie []byte
}

func ListenExtOr(network string, addr *net.TCPAddr, authCookie []byte) (net.Listener, error) {
	inner, err := net.ListenTCP(network, addr)
	ln := &ExtOrListener{Listener: inner, authCookie: authCookie}
	return ln, err
}

func (l *ExtOrListener) Accept() (net.Conn, error) {
	conn, err := l.Listener.Accept()
	if err != nil {
		return conn, err
	}
	//Parse out ExtOR info
	conn2, err := NewExtOrConn(conn, l.authCookie)
	if err != nil {
		conn.Close()
	}
	return conn2, err
}

type ExtOrConn struct {
	net.Conn
	remoteAddr net.Addr
}

func NewExtOrConn(conn net.Conn, authCookie []byte) (net.Conn, error) {

	extOrConn := &ExtOrConn{Conn: conn}

	//Parse out ExtOr info
	err := extOrPortAuthenticate(conn, authCookie)
	if err != nil {
		return nil, err
	}

	addr, err := extOrPortGetMetadata(conn)
	if err != nil {
		return nil, err
	}
	extOrConn.remoteAddr = addr

	return extOrConn, nil
}

func (c *ExtOrConn) RemoteAddr() net.Addr {
	return c.remoteAddr
}

// Read auth types. 217-ext-orport-auth.txt section 4.1.
// The server side of extOrPortAuthenticate from https://gitweb.torproject.org/pluggable-transports/goptlib.git
func extOrPortAuthenticate(s io.ReadWriter, authCookie []byte) error {
	//Send auth type SAFE_COOKIE (1)
	_, err := s.Write([]byte{1, 0})
	if err != nil {
		return err
	}

	//Read client response
	b := make([]byte, 1)
	_, err = io.ReadFull(s, b)
	if err != nil {
		return err
	}

	if b[0] != 1 {
		return fmt.Errorf("client didn't select auth type 1")
	}

	clientNonce := make([]byte, 32)
	clientHash := make([]byte, 32)
	serverNonce := make([]byte, 32)
	serverHash := make([]byte, 32)

	_, err = io.ReadFull(s, clientNonce)
	if err != nil {
		return err
	}
	_, err = io.ReadFull(rand.Reader, serverNonce)
	if err != nil {
		return err
	}

	serverHash = computeServerHash(authCookie, clientNonce, serverNonce)
	_, err = s.Write(serverHash)
	if err != nil {
		return err
	}
	_, err = s.Write(serverNonce)
	if err != nil {
		return err
	}
	_, err = io.ReadFull(s, clientHash)
	if err != nil {
		return err
	}

	// Compute expected client hash
	expectedClientHash := computeClientHash(authCookie, clientNonce, serverNonce)
	if subtle.ConstantTimeCompare(clientHash, expectedClientHash) != 1 {
		_, err := s.Write([]byte{0})
		if err != nil {
			return err
		}
		return fmt.Errorf("mismatch in client hash")
	}
	_, err = s.Write([]byte{1})
	if err != nil {
		return err
	}
	return nil

}

// The server side of extOrPortSetMetadata from https://gitweb.torproject.org/pluggable-transports/goptlib.git
func extOrPortGetMetadata(s io.ReadWriter) (net.Addr, error) {

	var addr *net.IPAddr
	for {
		cmd, body, err := extOrPortRecvCommand(s)
		if err != nil {
			if inerr := extOrPortSendCommand(s, extOrCmdDeny, []byte{}); inerr != nil {
				return nil, inerr
			}
			return nil, err
		}

		if cmd == extOrCmdUserAddr {
			host, _, err := net.SplitHostPort(string(body))
			if err != nil {
				return nil, err
			}
			addr, err = net.ResolveIPAddr("ip", host)
			if err != nil {
				return nil, err
			}
		} else if cmd == extOrCmdDone {
			break
		}
	}

	err := extOrPortSendCommand(s, extOrCmdOkay, []byte{})
	if err != nil {
		return nil, err
	}
	return addr, nil

}

// Generate and write an auth cookie to file
func writeAuthCookie(f io.Writer) ([]byte, error) {
	authCookieHeader := []byte("! Extended ORPort Auth Cookie !\x0a")

	n, err := f.Write(authCookieHeader)
	if err != nil {
		return nil, err
	}
	if n != 32 {
		return nil, fmt.Errorf("did not write full header")
	}

	cookie := make([]byte, 32)
	_, err = io.ReadFull(rand.Reader, cookie)
	if err != nil {
		return nil, err
	}

	n, err = f.Write(cookie)
	if err != nil {
		return nil, err
	}
	if n != 32 {
		return nil, fmt.Errorf("did not write full header")
	}

	return cookie, nil
}

// Write the contents of an auth cookie file. Takes as input the 32-byte
// cookie. See section 4.2.1.2 of 217-ext-orport-auth.txt.
func writeAuthCookieFile(filename string) ([]byte, error) {
	f, err := os.OpenFile(filename, os.O_RDWR|os.O_CREATE|os.O_EXCL, 0644)
	if err != nil {
		return nil, err
	}
	defer func() {
		closeErr := f.Close()
		if err == nil {
			err = closeErr
		}
	}()

	return writeAuthCookie(f)
}
